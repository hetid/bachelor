#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <string> 
#include <cstring>
#include <conio.h>
#include <vector>
#include <sstream>
#include <GL/glut.h>
#include <ctime>
#include <stdio.h>

using namespace std;

int height = 0;
int step_x = 0, step_y = 0;
vector<vector<float> > matrix;		//Height matrix
int alpha = 0, beta = 0;

void reshape(int w, int h)
{
	if (h == 0)
		h = 1;
	float ratio = 1.0* w / h;
	// ���������� ������� ��������
	glMatrixMode(GL_PROJECTION);
	// Reset �������
	glLoadIdentity();
	// ���������� ���� ���������
	glViewport(0, 0, w, h);
	// ���������� ���������� �����������.
	gluPerspective(50, ratio, 1, 2500);
	// ��������� � ������
	glMatrixMode(GL_MODELVIEW);
}
void display()
{	
	/*static int axe_x=0, axe_y=0, axe_z=0;
	char axes;
	cout << "Input axes = ";
	cin >> axes;
	switch (axes) {
	case 'x':
		cout << "Input x: ";
		cin >> axe_x;
		break;
	case 'y':
		cout << "Input y: ";
		cin >> axe_y;
		break;
	case 'z':
		cout << "Input z: ";
		cin >> axe_z;
		break;
	}*/

	int step = 3;
	
	glClear(GL_COLOR_BUFFER_BIT);
	glPushMatrix();
	glRotatef(-50, 1, 0, 0);
	glTranslatef(0, 950, -800);
	//glClear(GL_COLOR_BUFFER_BIT);
	//glPushMatrix();
	////glTranslatef(500, 500, -900);
	//glRotatef(-30, 1, 0, 0);
	//
	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	//gluLookAt(axe_x, axe_y, axe_x, 0, 0, 0, 0, 100, 0);
	//glTranslatef(0, 0, -800);
	////glRotatef(-45, 1, 0, 0);
	
	for (int x = -height / 2, column = 0; x < height / 2; x += step, column+=step)
	{
		glBegin(GL_LINE_STRIP);
		for (int y = -((int)matrix.size() / 2), str = 0; y < ((int)matrix.size() / 2); y += step, str+= step)
		{
			/*if (matrix[str][column] <= 50)
			{
				glColor3f(0.2, 0.2, 0.2);
			}
			else
			{
				glColor3f(0.8, 0.8, 0.8);
			}*/
			//glColor3f((matrix[str][column] / 100) - 0.1, (1 - matrix[str][column] / 200) - 0.1, 0);

			if (matrix[str][column] < 77) {
				glColor3f(157.0f / 255.0f, 232.0f / 255.0f, 251.0f / 255.0f); // -200 - -2000
			}
			else if (matrix[str][column] < 123) {
				glColor3f(10.0f / 255.0f, 113.0f / 255.0f, 0.0f / 255.0f); // 0 - -200
			}
			else if (matrix[str][column] < 169) {
				glColor3f(253.0f / 255.0f, 236.0f / 255.0f, 26.0f / 255.0f); // 0 - 200
			}
			else if (matrix[str][column] < 215) {
				glColor3f(249.0f / 255.0f, 130.0f / 255.0f, 0.0f / 255.0f); // 200 - 500
			}
			else if (matrix[str][column] < 261) {
				glColor3f(217.0f / 255.0f, 62.0f / 255.0f, 5.0f / 255.0f); // 500 - 1000
			}
			else {
				glColor3f(194.0f / 255.0f, 8.0f / 255.0f, 0.0f / 255.0f); // 1000 - 2000
			}

			glVertex3f(x, y, matrix[str][column]);
		}
		glEnd();
	}
	for (int y = -((int)matrix.size() / 2), str = 0; y < ((int)matrix.size() / 2); y += step, str+= step)
	{
		glBegin(GL_LINE_STRIP);
		for (int x = -height / 2, column = 0; x < height / 2; x += step, column+= step)
		{
			/*if (matrix[str][column] <= 50)
			{
				glColor3f(0.2, 0.2, 0.2);
			}
			else
			{
				glColor3f(0.8, 0.8, 0.8);
			}*/
			//glColor3f((matrix[str][column] / 100) - 0.1, (1 - matrix[str][column] / 200) - 0.1, 0);

			if (matrix[str][column] < 77) {
				glColor3f(157.0f / 255.0f, 232.0f / 255.0f, 251.0f / 255.0f); // -200 - -2000
			}
			else if (matrix[str][column] < 123) {
				glColor3f(10.0f / 255.0f, 113.0f / 255.0f, 0.0f / 255.0f); // 0 - -200
			}
			else if (matrix[str][column] < 169) {
				glColor3f(253.0f / 255.0f, 236.0f / 255.0f, 26.0f / 255.0f); // 0 - 200
			}
			else if (matrix[str][column] < 215) {
				glColor3f(249.0f / 255.0f, 130.0f / 255.0f, 0.0f / 255.0f); // 200 - 500
			}
			else if (matrix[str][column] < 261) {
				glColor3f(217.0f / 255.0f, 62.0f / 255.0f, 5.0f / 255.0f); // 500 - 1000
			}
			else {
				glColor3f(194.0f / 255.0f, 8.0f / 255.0f, 0.0f / 255.0f); // 1000 - 2000
			}

			glVertex3f(x, y, matrix[str][column]);
		}
		glEnd();
	}
	glPopMatrix();
	glutSwapBuffers();
}
void print_matrix(vector<vector<float> > &mtrx) {
	for (int i = 0; i < mtrx.size(); i++)
	{
		for (int j = 0; j < mtrx[i].size(); j++)
		{
			cout << mtrx[i][j] << ' ';
		}
		cout << endl;
	}
}
void processing(const char *raw_data)
{
	string r_str(raw_data);
	istringstream s_in(r_str);
	bool height_is_find=false;
	unsigned int x_first = 0, y_first=0;
	while (!height_is_find)
	{
		float num_temp;
		for (int i = 0; i<3; i++)
		{
			s_in >> num_temp;

			if (height == 0)
			{
				x_first = num_temp;
			}

			if (i == 0 && x_first == num_temp)
			{
				height++;
			}
			if (i == 0 && x_first != num_temp)
			{
				height_is_find = true;
				step_x = num_temp - x_first;
				//cout << "x_first = " << x_first << "; num_temp = " << num_temp << endl;
			}
			
			if (height == 1 && i == 1)
			{
				y_first = num_temp;
			}
			if (height == 2 && i == 1)
			{
				step_y = num_temp - y_first;
				//cout << "y_first = " << y_first << "; num_temp = " << num_temp << endl;
			}

		}
	}
}
void read_matrix(const char *raw_data)
{
	string r_str(raw_data);
	istringstream s_in(r_str);
	while (!s_in.eof())
	//for(int j = 0; j < 200; j++)
	{
		float x, y, z;
		vector<float> temp_m_line;
		temp_m_line.clear();
		for (unsigned int i = 0; i<height; i++)
		{
			s_in >> x >> y >> z;
			temp_m_line.push_back(z);
		}
		matrix.push_back(temp_m_line);
	}
}
void animate(void) 
{
	glutPostRedisplay();
}
void changeSize(int w, int h) 
{
	// �������������� ������� �� ����
	if (h == 0)
		h = 1;
	float ratio = w * 1.0 / h;
	// ������� ��������
	glMatrixMode(GL_PROJECTION);
	// �������� �������
	glLoadIdentity();
	glViewport(0, 0, w, h);
	gluPerspective(45.0f, ratio, 0.1f, 100.0f);
	// ������� ��������
	glMatrixMode(GL_MODELVIEW);
}

void GL_init(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(1366, 768);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("try1");
	glClearColor(0, 0, 0, 1.0);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-1, 1, -1, 1, 1, 2000);

	glMatrixMode(GL_MODELVIEW);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	
	glutIdleFunc(animate);
	


	glutMainLoop();
}

int main(int argc, char * argv[])
{
	setlocale(LC_ALL, "Russian");
	const char *file_name = "coord.txt";

	//�������� �����

	ifstream fin;
	fin.open(file_name);
	if (!fin.is_open())					//File validation
	{
		cout << "������ �����";
		return 1;
	}

	//����������� ������� �����
	unsigned int file_size;
	fin.seekg(0, ios::end);
	file_size = fin.tellg();
	fin.close();

	//���������� �����
	char *raw_data = new char[file_size + 1];
	FILE *f;
	f = fopen(file_name, "rb");
	fread(raw_data, file_size, 1, f);
	fclose(f);
	raw_data[file_size] = '\0';

	unsigned int start_time = clock();
	processing(raw_data);
	cout << "��� �� ��� X = " << step_x << "; ��� �� ��� Y = " << step_y << "; ����� ������ = " << height << endl;

	read_matrix(raw_data);
	cout << "���-�� �������� = " << height << "; ���-�� ����� = " << matrix.size() << endl;
	
	unsigned int end_time = clock();
	cout << "����� ������ � ��������� ������ = " << (end_time - start_time) / 1000 << " ������ ";

	//print_matrix(matrix);
	//////////////////
	//	test GLUT	//
	//////////////////

	GL_init(argc, argv);

	//////////////////
	//	end test	//
	//////////////////
	
	_getch();
	return(0);
}